/**
 * AMALYZE API
 * AMALYZE API 
 *
 * OpenAPI spec version: 0.0.12
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { InlineResponse200Campaigns } from './inlineResponse200Campaigns';
import { PaginationResponse } from './paginationResponse';
import { Request } from './request';


export interface InlineResponse200 { 
    pagination?: PaginationResponse;
    request?: Request;
    campaigns?: Array<InlineResponse200Campaigns>;
}
