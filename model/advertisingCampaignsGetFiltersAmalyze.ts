/**
 * AMALYZE API
 * AMALYZE API 
 *
 * OpenAPI spec version: 0.0.12
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { CampaignProgress } from './campaignProgress';


export interface AdvertisingCampaignsGetFiltersAmalyze { 
    /**
     * searches in campaign name or ASIN or SKU
     */
    generic?: string;
    progress?: CampaignProgress;
}
