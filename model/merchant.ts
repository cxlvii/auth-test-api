/**
 * AMALYZE API
 * AMALYZE API 
 *
 * OpenAPI spec version: 0.0.12
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


export interface Merchant { 
    /**
     * The merchant's id
     */
    id?: string;
    /**
     * The merchant's name
     */
    name?: string;
}
