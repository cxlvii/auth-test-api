/**
 * AMALYZE API
 * AMALYZE API 
 *
 * OpenAPI spec version: 0.0.12
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { PaginationSize } from './paginationSize';


export interface PaginationRequest { 
    /**
     * The requested datapage (incombination with `size`)
     */
    page?: number;
    size?: PaginationSize;
}
