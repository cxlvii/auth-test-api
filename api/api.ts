export * from './advertisingCampaigns.service';
import { AdvertisingCampaignsService } from './advertisingCampaigns.service';
export * from './generic.service';
import { GenericService } from './generic.service';
export * from './systemUser.service';
import { SystemUserService } from './systemUser.service';
export const APIS = [AdvertisingCampaignsService, GenericService, SystemUserService];
